const nodemailer = require('nodemailer');

const properties = require('../../../config/properties');

module.exports = {
    transporter : nodemailer.createTransport(properties.smtpURL)
};

