const properties = require('../../../../config/properties');
const transporter = require('../email-config');
/* const User = require('../../../users/dao/user.dao'); */

const User = require('../../../api/user/user.dao')



let randomFixedInteger = function (length) {
    return Math.floor(Math.pow(10, length-1) + Math.random() * (Math.pow(10, length) - Math.pow(10, length-1) - 1));
};

module.exports = { 
    sendcode : function(req, res, next){
        console.log("req.body",req.body);
        User.getByEmail({email : req.body.email}, function(err, user){
            console.log(user);
            if(err){
                console.log("Error",err);
                res.json({message : err});
            }

            if(!user){
               res.status(404).json({message : "User of that email id is not found"});
            }else{
                    randomCode = randomFixedInteger(6);

                    mailOptions = {
                        from : properties.senderEmail,
                        to : req.body.email,
                        subject : "Check Email Verification Code",
                        html : "<p>Hi <span style='color:cyan;font-weight:bolder;font-size:16px'>"+req.body.email+"</span>, please find your email verification code in this email<p> <br><div style='font-weight:bolder;font-size:42px;color:#000'>"+randomCode+"<div>"
                    };

                    transporter.transporter.sendMail(mailOptions, function(err, info){
                            if(err){
                                res.send(err);
                            }
                            res.json({
                                text: true,
                                message: "Email Verification code sends on your email",
                                key : user._id,
                                email : user.email
                            });
                        }); 
                }

        })
    },

    verifycode : function(req, res, next){
        console.log("randomCode",randomCode);
        console.log("req.body",req.body)
        if(parseInt(req.body.verificationcode) === randomCode){
            res.json({text:true,message : "Verification code is right"})
        }else{
            res.json({text:false,message : "Verification code is wrong"})
        }                  
    }
}