var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var userSchema = new Schema({
    fullname : {
        type : String,
        unique : false
    },
    username : {
        type : String,
        unique : true,
    },
    email : {
        type : String,
        unique : true,
    },
    password :{
       type : String,
        unique : false, 
    },
    profession: {
        type: String,
        unique : false,
        required : false
    },
    bio : {
        type: String,
        unique : false,
        required : false
    },
    dob : {
        type : Date,
        required : false,
        unique : false
    },
    mobile : {
        type : Number,
        unique : true,
        required : false
    },
    gender: {
        type: String,
        unique : false,
        required : false
    }
},{timestamps : true});

module.exports = userSchema;