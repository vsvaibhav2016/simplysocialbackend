var express = require('express');
var passport = require('passport');
var router = express.Router();
var User = require('./user.controller');


module.exports = function(router,passport){
    router.post('/signup',passport.authenticate('signup', {session: false}),User.signup);
    router.post('/login',passport.authenticate('login', {session: false}),User.login);
    router.get('/protected', passport.authenticate('jwt',{session : false}),User.profile);
    /* router.put('/uploaddp/:id',User.uploadPic); */
    router.put('/updateuser/:id', User.updateUser);
    router.put('/resetpassword/:email', User.resetpassword);
    router.put('/changepassword/:id',User.changePassword);
}

