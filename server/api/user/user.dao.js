var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var userSchema = require('./user.model');

userSchema.pre('save', function(next){
    var user = this;
    this.hashedPassword(user.password, function(err, hash){
        if(err){
            console.log("Error in pre save hook ", err);
            next(err)
        }

        user.password = hash;
        next();
    })
});

userSchema.methods.hashedPassword = function(password, cb){
        bcrypt.genSalt(15, function(err,salt){
            if(err){
                console.log("err in bcrypt.genSalt",err);
                return cb(err);
            }
        
        bcrypt.hash(password,salt,function(err, hashPassword){
            if(err){
                console.log("Error in bcrypt.hash", err);
                return cb(err);   
            }
                return cb(null,hashPassword);
        })
            
    });
        
};

userSchema.methods.comparePassword = function(password,hashedPassword,cb){
        bcrypt.compare(password, hashedPassword, function(err, isMatch){
            if(err){
                console.log("Error in comparing password ", err);
                return cb(err);
            }
            return cb(null, isMatch);
            
        })
    }

userSchema.statics = {
    hashPassword : function(password){
        return bcrypt.hashSync(password, bcrypt.genSaltSync(15),null);
    },

    comparepassword : function(password){
        return bcrypt.compareSync(password, this.password);
    }, 

    create : function(data, cb){
        var user = new this(data);
        user.save(cb);
    },

    getByEmail : function(query, cb){
        this.findOne(query,cb);
    },

    getById : function(query, cb){
        this.findById(query, cb);
    },

    update: function(query, updateData, cb){
        this.findOneAndUpdate(query,{$set : updateData},{new : true}, cb)
    }


}

var userModel = mongoose.model('User',userSchema);

module.exports = userModel;

