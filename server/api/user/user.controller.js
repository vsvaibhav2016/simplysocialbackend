var jwt = require('jsonwebtoken');
var User = require('./user.dao');
var config = require('../../../config/properties');

var multer = require('multer');
var fileUpload = require('../../../utilities/fileupload')(multer);

function generateToken(user){
    return jwt.sign(user, config.secret,{
        expiresIn : 10080
    });
}

function setUserInfo(request){
    return {
        _id : request._id,
        email : request.email,
        username : request.username,
        fullname : request.fullname
    }
}


exports.login = function(request, response, next){
    var user = setUserInfo(request.user);

    response.status(200).json({
        message : "Login Successfully",
        token : 'JWT '+ generateToken(user),
        user : user
    })
};

exports.signup = function(request, response, next){
    var user = setUserInfo(request.user);
    console.log("Signup user info",user);
    response.status(201).json({
        message : "Signup Successfully",
        token : 'JWT '+ generateToken(user),
        user : user
    })
}

exports.profile = function(request, response, next){
    var user = setUserInfo(request.user);
    console.log("Signup user info",user);
    response.json({
        message : "Protected Routes",
        statusText : "authenticated user",
        user : user
    })
}



exports.updateUser = function(request, response, next){
    User.update({_id : request.params.id}, request.body, function(err, user){
        if(err){
            console.log("Error in updating user", err);
            response.send(err);
        }
        
        response.json(user);
    })
}

exports.resetpassword = function(request, response, next){
    request.body.password= User.hashPassword(request.body.password)
    User.update({email : request.params.email}, request.body, function(err, user){
        console.log("request.params",request.params.email);
        console.log(" request.body",request.body);
        
        if(err){
            console.log("Error in updating user", err);
            response.send(err);
        }
        
        response.json(user);
    })
}


exports.changePassword = function(request, response, next){
    User.getById({_id : request.params.id}, function(err, user){
        if(err){
            response.json({
                text : false,
                message: err
            })
        }else{
            console.log(request.body);
            console.log("user.password",user.password);/* 
            console.log("fetch",User.comparepassword(request.body.oldpassword)); */
            if(user.password === User.hashPassword(request.body.oldpassword)){
                response.json({message:"oldpassword is matched"});
            }else{
                response.json({message:"oldpassword is not matched"});
            }
        }
    })
}