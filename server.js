var express = require('express');
var log = require('morgan')('dev');
var bodyParser = require('body-parser');
var passport = require('passport');

var config = require('./config/properties');
var db = require('./config/database');
var Userroutes = require('./server/api/user/user.routes');
var verificationRoutes = require('./server/utils/mailer/verificationcode/verificationcode.routes');
var setupPassport = require('./config/passport');
var port = config.port;
var app = express();


var bodyParserJSON = bodyParser.json();
var bodyParserURLEncoded = bodyParser.urlencoded({extended:true});
var router = express.Router();
db();



app.use(log);
app.use(bodyParserJSON);
app.use(bodyParserURLEncoded);
app.use(passport.initialize())
app.use(passport.session());

app.use(function(req, res, next) {
   res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Credentials", "true");
    res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Origin,Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,Authorization");
  next();
});

setupPassport(passport);
app.use('/api',router);

verificationRoutes(router);
Userroutes(router,passport);

// demo master push

app.listen(port, function(request, response){
    console.log("Server is running on "+ port + " port");
});