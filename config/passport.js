var passport = require('passport');
var User = require('../server/api/user/user.dao');
var local = require('./strategies/local');
var jwt = require('./strategies/jwt');

module.exports = function(){
    passport.serializeUser(function(user, done){
        done(null, user._id);    
    });

    passport.deserializeUser(function(id, done){
        User.getById({_id : id}, function(err, user){
            if(err){
                console.log("Error in deserialize the user", err);
                throw err;
            }
            done(null, user);
        })
    })

    passport.use(jwt);
    passport.use('signup',local.localSignup);
    passport.use('login',local.localLogin);
}