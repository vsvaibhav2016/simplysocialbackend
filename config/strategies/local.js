var passport = require('passport');
var local = require('passport-local');
var localStrategy = local.Strategy;

var User = require('../../server/api/user/user.dao');

var localOptions = {
    usernameField : 'email',
    passReqToCallback : true
}

module.exports = {
    localLogin : new localStrategy(localOptions, function(req,email,password,done){
        User.getByEmail({
            email : email
        },function(err, user){
            if(err){
                console.log("Error in find user of same username", err);
                return done(err);
            }

            if(!user){
                console.log("User of that email id is not found");
                return done(null, false, {error : "User of that email id is not found"});
            }

            user.comparePassword(password, user.password, function(err, isMatch){
                 if(err){
                        return done(null, false, {message: err});
                    }

                if(!isMatch){
                        return done(null, false, {message: "password is incorrect"});
                    }

                    return done(null,user);
            })

        })
    }),

    localSignup : new localStrategy(localOptions, function( req, email, password, done){
       
        User.getByEmail({
            email : email
        },function(err, user){
            if(err){
                    console.log("Error in find user of same username", err);
                    return done(err);
                }
            if(user){
                return done(null, false);
            }else{
                
                var data = { email : email, password : password, username: req.body.username, fullname: req.body.fullname }
                User.create(data, function(err, data){
                    if(err){
                        console.log("Error in registering user");
                        throw err
                    }
                    return done(null, data);
                })
            }
        })
    })


}

