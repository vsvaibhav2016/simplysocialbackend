var JWT = require('passport-jwt');
var JWTStrategy = JWT.Strategy;
var ExtractJWT = JWT.ExtractJwt;
var properties = require('../properties');
var User = require('../../server/api/user/user.dao');

var JWTOptions = {
    jwtFromRequest : ExtractJWT.fromAuthHeader(),
    secretOrKey : properties.secret
}

module.exports = new JWTStrategy(JWTOptions, function(payload, done){
    console.log("payload",payload);
    User.getById({_id : payload._id}, function(err, user){
        console.log("user",user);
        if(err){
            return done(err, false);
        }

        if(user){
            done(null, user)
        }else{
            done(null, false)
        }
    })
})